import * as PIXI from 'pixi.js'

/**
 * Usage
 * 
 * Imports every valid asset and saves its reference
 * in exported (default) object tree
 * /assets/atlas/main/atlas.gen -> default.atlas.main 
 * /assets/sounds/ball.mp3 -> default.sounds.ball 
 * 
 * Asset names should be named as such
 * [locale.]?[name][.ext]?
 * 
 * locale and ext parts will be droped  
 * assets with invalid 
 */

const assets = {}
assets.load = (pixiLoader = PIXI.loader, locale = 'en') => {
  function addAll (r) {
    r.keys().forEach(key => {
      const asset = r(key).default
      const path = key.split('/').slice(1)
      
      if (path[path.length - 1].endsWith('.gen')) {
        path.pop()
      }
      
      let t = assets
      for (let i = 0; i < path.length - 1; i++) {
        const p = path[i]

        if (!t[p]) {
          t[p] = {}
        }

        t = t[p]
      }

      const parts = path[path.length - 1].split('.')

      
      let assetLocale = null
      if (parts.length > 2 && parts[0].length === 2) {  // has locale
        assetLocale = parts.shift()
      } 

      const assetName = parts[0]
            
      if (!assetLocale || assetLocale === locale) {        
        // TODO: add verbose mode
        //console.log(`addding (${path.join('/')}) ${assetName} with locale: ${assetLocale}`)
        t[assetName] = asset
        pixiLoader.add(asset)
      }
    });
  }
  
  addAll(require.context('./', true, /\.(json|gen|mp3|wav|fnt)$/)); // eslint-disable-line


  return new Promise(resolve => {
    pixiLoader.load(() => resolve())
  })
}

export default assets