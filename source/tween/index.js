// TODO: избавится от деревовидной структуры update-tree
// идея - для parallel/chain использовать группу (Group) которая обновляет 
// их последовательно

// TODO: everything to milliseconds

import TWEEN from '@tweenjs/tween.js'

const noop = {}
const noopGroup = {
  noop: true,
  add: () => { },
  remove: () => { }
}

function tween(object, props, seconds, opts) {
  const t = new TWEEN.Tween(object)
    .to(props, seconds * 1000)

  if (opts) {
    opts.easing && t.easing(opts.easing)
    opts.repeat && t.repeat(opts.repeat)
    opts.yoyo && t.yoyo(opts.yoyo)
    opts.delay && t.delay(opts.delay * 1000)
    opts.onStart && t.onStart(opts.onStart)
    opts.onUpdate && t.onUpdate(opts.onUpdate)
    opts.onStop && t.onStop(opts.onStop)
    opts.onComplete && t.onComplete(opts.onComplete)
    opts.onRepeat && t.onRepeat(opts.onRepeat)
  }

  return t
}

class Base {
  constructor(...tweens) {
    this._tweens = []
    this._group = TWEEN

    this._isPlaying = false
    this._startTime = null
    this._id = TWEEN.nextId()
    this._duration = 0
    this._onCompleteCallback = null

    this._repeat = 0
    this._delayTime = 0
    this._repeating = false

    for (const arg of tweens) {
      this.add(arg)
    }
  }

  delay(value) {
    this._delayTime = value
    return this
  }

  getId () {
    return this._id
  }

  onStart (cb)  {
    this._onStartCallback = cb

    return this
  }

  onComplete(cb)  {
    this._onCompleteCallback = cb

    return this
  }

  onUpdate (cb) {
    this._onUpdateCallback = cb
    return this
  }

  onRepeat (cb)  {
    this._onRepeatCallback = cb
    return this
  }


  wait(seconds) {
    this.tween(noop, noop, seconds)

    return this
  }


  call(func) {
    this.tween(noop, noop, 0, { onComplete: func })

    return this
  }

  tween(object, props, seconds, opts) {
    this.add(tween(object, props, seconds, opts))

    return this
  }


  add(t) {
    if (t === this)
      throw new Error(`Can't add chain tween to itself`)

    if (t._group != TWEEN && !t._group.noop)
      throw new Error(`Chaining tweens from groups are not supported`)

    if (t._startTime !== null)
      throw new Error(`Can't add started tween`)

    t._group = noopGroup

    this._tweens.push(t)

    return this
  }

  repeat(times) {

    this._repeat = times;
    return this;

  }

  start(time) { }
  update(time) { }
  stop() { }
}

class Chain extends Base {
  constructor(...tweens) {
    super(...tweens)
    this._tween = 0
  }

  update (time) {
    time = time !== undefined ? time : TWEEN.now();

    if (this._tween < this._tweens.length) {
      let lead = null;
      while ((lead = this._tweens[this._tween]).update(time) === false) {
        lead._isPlaying = false;
        this._duration += lead._duration

        this._tween++

        if (this._repeat > 0 && this._tween === this._tweens.length) {
          this._tween = 0
          this._repeating = true

          if (isFinite(this._repeat))
            this._repeat--

          this._onRepeatCallback && this._onRepeatCallback()
        }


        if (this._tween < this._tweens.length) {
          const next = this._tweens[this._tween]
          if (this._repeating) {
            // TODO: revise this logic to improve precision

            // restartTween(next, lead._startTime + lead._duration)
            // do not rely on _duration since not every tween will provide it properly
            // so we will sacrifice some precision on part-frame starts
            restartTween(next, time)
          } else {
            // next.start(lead._startTime + lead._duration)
            // do not rely on _duration since not every tween will provide it properly
            // so we will sacrifice some precision on part-frame starts
            next.start(time)
          }
        } else {
          break
        }
      }

    }

    this._onUpdateCallback && this._onUpdateCallback()

    const active = this._tween < this._tweens.length

    if (!active) {
      this._onCompleteCallback && this._onCompleteCallback()
    }

    return active
  }

  start(time) {

    if (this._isPlaying)
      return this

    this._onStartCallback && this._onStartCallback()
    
    this._group.add(this)

    this._startTime = time !== undefined ? typeof time === 'string' ? TWEEN.now() + parseFloat(time) : time : TWEEN.now();
    this._startTime += this._delayTime

    this._isPlaying = true

    if (this._tweens.length > 0) {
      this._tweens[0].start(this._startTime)
    }

    return this
  }

  stop () {
    if (!this._isPlaying) {
      return this;
    }

    for (const t of this._tweens) {
      t.stop()
    }

    this._group.remove(this)
    this._isPlaying = false

    return this

  }
}

class Parallel extends Base {
  constructor(...tweens) {
    super(...tweens)

    this._delayOffset = null
  }

  delayOffset(value) {
    this._delayOffset = value * 1000

    return this
  }

  add(t) {
    return super.add(t)
  }

  update = (time) => {
    time = time !== undefined ? time : TWEEN.now();

    let active = false
    for (const t of this._tweens) {
      if (!t || !t._isPlaying)
        continue

      if (t.update(time) === false) {
        t._isPlaying = false
        this._duration = t._duration
      } else {
        active = true
      }
    }

    if (!active) {
      if (this._repeat > 0) {
        active = true

        if (isFinite(this._repeat))
          this._repeat--

        for (const t of this._tweens) {
          restartTween(t)
        }

      } else {
        this._onCompleteCallback && this._onCompleteCallback()
      }
    }

    return active
  }

  start = (time) => {
    if (this._isPlaying)
      return this

    this._onStartCallback && this._onStartCallback()

    this._group.add(this)

    this._startTime = time !== undefined ? typeof time === 'string' ? TWEEN.now() + parseFloat(time) : time : TWEEN.now();
    this._startTime += this._delayTime
    this._isPlaying = true    

    let delay = 0.0
    for (const t of this._tweens) {
      t.start(this._startTime + delay)
      if (this._delayOffset) {
        delay += this._delayOffset        
      }
    }

    

    return this
  }

  stop = () => {
    if (!this._isPlaying) {
      return this;
    }

    for (const t of this._tweens) {
      t.stop()
    }

    this._group.remove(this)
    this._isPlaying = false

    return this
  }
}

function restartTween(tween, now) {
  if (tween instanceof Chain || tween instanceof Parallel)
    throw new Error('Unable to reset Chain! (repeating subchains?)')

  for (let property in tween._valuesStartRepeat) {

    if (typeof (tween._valuesEnd[property]) === 'string') {
      tween._valuesStartRepeat[property] = tween._valuesStartRepeat[property] + parseFloat(tween._valuesEnd[property]);
    }

    tween._valuesStart[property] = tween._valuesStartRepeat[property];
  }

  tween._startTime = now
}




export const Easing = TWEEN.Easing
export const easing = TWEEN.Easing
tween.Easing = TWEEN.Easing
tween.easing = TWEEN.Easing

tween.from = (target, from, duration, options) => {
  const to = {}
  for (const key of Object.keys(from)) {
    to[key] = target[key]
    target[key] = from[key]
  }

  return tween(target, to, duration, options)
}

tween.chain = (...tweens) => new Chain(...tweens)
tween.parallel = (...tweens) => new Parallel(...tweens)
tween.noop = (seconds = 0) => tween(noop, noop, seconds)
tween.update = TWEEN.update.bind(TWEEN)

Object.defineProperty(tween, 'getTime', {
  set: value => TWEEN.now = value,
  get: () => TWEEN.now
})

export default tween


