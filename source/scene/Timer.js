import * as PIXI from 'pixi.js'
import Sprite from '@/game/z/Sprite';
import atlas from '@/assets/images'
import bungee from '@/assets/fonts/bungee.fnt'
import game from '@/game';

export default class Timer extends Sprite {
  constructor() {
    super(atlas.getTexture('timer_bg'))

    this.time = 0.0
    this.timeLeft = 15

    const text = new PIXI.extras.BitmapText('0:00', { font: bungee, tint: 0xFFFFFF })
    text.anchor.set(.5, .5)
    text.position.set(222, 77)
    this.addChild(text)

    game.onEnterFrame.add(delta => {
      this.time += delta
      this.timeLeft = Math.max(0, this.timeLeft - delta)

      const minutes = String(Math.floor(this.timeLeft / 60))
      const seconds = String(Math.ceil(this.timeLeft) % 60).padStart(2, '0')
      text.text = `${minutes}:${seconds}`
    })
  }
}