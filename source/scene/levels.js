import atlas1 from '@/assets/levels/1/atlas.gen'
import level1 from '@/assets/levels/1/level.json'
import atlas2 from '@/assets/levels/2/atlas.gen'
import level2 from '@/assets/levels/2/level.json'

import atlas3 from '@/assets/levels/3/atlas.gen'
import level3 from '@/assets/levels/3/level.json'

import atlas4 from '@/assets/levels/4/atlas.gen'
import level4 from '@/assets/levels/4/level.json'

import atlas5 from '@/assets/levels/5/atlas.gen'
import level5 from '@/assets/levels/5/level.json'

import atlas6 from '@/assets/levels/6/atlas.gen'
import level6 from '@/assets/levels/6/level.json'

import atlas7 from '@/assets/levels/7/atlas.gen'
import level7 from '@/assets/levels/7/level.json'

import atlas8 from '@/assets/levels/8/atlas.gen'
import level8 from '@/assets/levels/8/level.json'

import atlas9 from '@/assets/levels/9/atlas.gen'
import level9 from '@/assets/levels/9/level.json'

import atlas10 from '@/assets/levels/10/atlas.gen'
import level10 from '@/assets/levels/10/level.json'

export default [
  [level1, atlas1],
  [level2, atlas2],
  [level3, atlas3],
  [level4, atlas4],
  [level5, atlas5],
  [level6, atlas6],
  [level7, atlas7],
  [level8, atlas8],
  [level9, atlas9],
  [level10, atlas10]
]