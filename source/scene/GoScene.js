import * as PIXI from 'pixi.js'
import images from '@/assets/images'
import game from '@/game'

export default class GoScene extends PIXI.Container {
  static permissionGranted = false

  constructor(onPermissionGranted) {
    super()

    const proceed = () => {
      GoScene.permissionGranted = true      
      this.destroy()   

      onPermissionGranted()
    }

    const goButton = new PIXI.Sprite(images.getTexture('go'))
    goButton.interactive = true
    goButton.buttonMode = true
    goButton.anchor.set(.5, .5)
    goButton.position.set(game.width * .5, game.height * .5)
    goButton.on('pointertap', () => {
      openFullscreen()

      const DeviceMotionEvent = window.DeviceMotionEvent
      if (DeviceMotionEvent && DeviceMotionEvent.requestPermission) {
        DeviceMotionEvent.requestPermission()
          .then(responce => {                        
            proceed()            
          })
      } else {
        proceed()
      }
    })    
    this.addChild(goButton)

  }

} 


function openFullscreen() {
  const elem = document.body  
  
  if (elem.requestFullscreen) {
    elem.requestFullscreen();
  } else if (elem.mozRequestFullScreen) { /* Firefox */
    elem.mozRequestFullScreen();
  } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
    elem.webkitRequestFullscreen();
  } else if (elem.msRequestFullscreen) { /* IE/Edge */
    elem.msRequestFullscreen();
  }
}