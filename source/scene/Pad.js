import * as PIXI from 'pixi.js'
import Sprite from '@/game/z/Sprite'
import atlas from '@/assets/images'
import game from '@/game'

export default class Pad extends Sprite {
  constructor() {
    super(atlas.getTexture('pad'))

    this.xValue = 0
    this.yValue = 0


    this.interactive = true

    const lp = new PIXI.Point()
    const hw = this.width * .5
    const hh = this.height * .5
    const updateValues = (event) => {
      if (down) {
        event.data.getLocalPosition(this, lp)
        
        this.xValue = Math.max(-1, Math.min(1, (lp.x - hw) / hw))
        this.yValue = Math.max(-1, Math.min(1, (lp.y - hh) / hh))
      } else {
        this.xValue = 0
        this.yValue = 0
      }
      
    }

    let down = false

    this.on('pointerdown', event => {
      down = true
      updateValues(event)
      
    })

    this.on('pointermove', (event) => {
      updateValues(event)
    })


    const cancel = (event) => {
      down = false
      updateValues(event)
    }
    this.on('pointercancel', cancel)
    this.on('pointerup', cancel)
    this.on('pointerupoutside', cancel)
    
  }
}