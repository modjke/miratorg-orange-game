import * as PIXI from 'pixi.js'
import game from '@/game'

import images from '@/assets/images/atlas.gen'


import createSegment from './core/createSegment'
import Sprite from '@/game/z/Sprite'
import ZContainer from '@/game/z/Container'

import pk, { Edge } from 'planck-js'
import Orange from './core/Orange'
import Timer from './Timer'
import Pad from './Pad'

import levels from './levels'

const PSCALE = 0.001

function updateZIndex(sprite) {
  sprite.zIndex = sprite.y + sprite.height * (1 - sprite.anchor.y)
}


export default class SensorScene extends PIXI.Container {
  constructor(onLevelComplete) {
    super()
    
    game.stage.interactive = true
    

    const { width, height } = game

    const forceLevel = window.MiratorgGame ? window.MiratorgGame.forceLevel : null
    const levelIndex = typeof forceLevel === 'number' ? forceLevel : Math.floor(levels.length * Math.random())
    const [ level, atlas ] = levels[levelIndex]
    this.segment = createSegment(level, atlas)

    const levelContainer = this.addChild(new ZContainer())
    levelContainer.sortableChildren = true

    const timer = new Timer()
    timer.position.set(60, 60)
    this.addChild(timer)

    this.pad = new Pad()    
    this.pad.position.set(1500, 690)
    this.addChild(this.pad)


    for (const sprite of this.segment.background) {
      sprite.zIndex = -20
      levelContainer.addChild(sprite)
    }

    
    
    
    const orangeShadow = new Sprite(images.getTexture('shadow'))
    orangeShadow.zIndex = -6    
    orangeShadow.alpha = 0.3
    orangeShadow.anchor.set(.45, .45)
    orangeShadow.scale.set(1.35, 1.35)
    levelContainer.addChild(orangeShadow)

    const orange = new Orange()
    levelContainer.addChild(orange)

    for (const item of this.segment.items) {
      updateZIndex(item)
      levelContainer.addChild(item)
    }

    for (const item of this.segment.foreground) {
      item.zIndex = 99999
      levelContainer.addChild(item)
    }

    const world = pk.World({ gravity: pk.Vec2(0, 0) })
    const obstacles = world.createBody({
      type: 'static',
      position: pk.Vec2(0, 0)
    })
    const edges = {
      minX: 0,
      minY: 0,
      maxX: 0,
      maxY: 0
    }
    for (const coord of level.static) {
      const verts = [
        pk.Vec2(coord.left * PSCALE, coord.top * PSCALE),
        pk.Vec2(coord.right * PSCALE, coord.top * PSCALE),
        pk.Vec2(coord.right * PSCALE, coord.bottom * PSCALE),
        pk.Vec2(coord.left * PSCALE, coord.bottom * PSCALE)
      ]

      obstacles.createFixture(pk.Polygon(verts))

      for (const v of verts) {
        edges.minX = Math.min(edges.minX, v.x)
        edges.minY = Math.min(edges.minY, v.y)
        edges.maxX = Math.max(edges.maxX, v.x)
        edges.maxY = Math.max(edges.maxY, v.y)
      }
    }

    const padding = 50 * PSCALE
    edges.minX -= padding
    edges.maxX += padding
    edges.minY -= padding
    edges.maxY += padding

    

    obstacles.createFixture(pk.Edge(pk.Vec2(edges.minX, edges.minY), pk.Vec2(edges.maxX, edges.minY)))
    // obstacles.createFixture(pk.Edge(pk.Vec2(edges.maxX, edges.minY), pk.Vec2(edges.maxX, edges.maxY)))    
    obstacles.createFixture(pk.Edge(pk.Vec2(edges.maxX, edges.maxY), pk.Vec2(edges.minX, edges.maxY)))    
    obstacles.createFixture(pk.Edge(pk.Vec2(edges.minX, edges.maxY), pk.Vec2(edges.minX, edges.minY)))
    
    
    const floor = new PIXI.extras.TilingSprite(images.getTexture('floor_tile'), levelContainer.width, 1080)
    floor.zIndex = -25
    levelContainer.addChild(floor)

    const orangeInital = pk.Vec2(0 * PSCALE, 593 * PSCALE)
    const orangeInitalVel = pk.Vec2(0.2, 0)

    const orangeBody = world.createBody({
      type: 'dynamic',
      allowSleep: false,
      position: orangeInital,
      linearDamping: 0.5
    })

    orangeBody.createFixture(pk.Circle(pk.Vec2(0, 0), orange.height * .40 * PSCALE), {
      density: 1,
      friction: 0
    })

    function resetOrange() {
      orangeBody.setPosition(orangeInital)
      orangeBody.setLinearVelocity(orangeInitalVel)
      gravity.set(zero)        
      timer.time = 0.0
    }

    let alpha = 0
    let beta = 0
    let gamma = 0

    let gyroEnabled = false

    this._handleDeviceOrientation = event => {
      alpha = event.alpha
      beta = event.beta
      gamma = event.gamma

      if (event.alpha || event.beta || event.gamma) {
        gyroEnabled = true
        this.pad.visible = false
      }
    }

    window.addEventListener('deviceorientation', this._handleDeviceOrientation)

    const levelWidth = levelContainer.width - 80
    const orangeLevelEnd = levelContainer.width + 50
    const zero = pk.Vec2(0, 0)
    const gravity = pk.Vec2(0, 0)
    const diff = pk.Vec2(0, 0)
    
    levelContainer.x = game.viewport.left

    resetOrange()
    this._onEnterFrame = game.onEnterFrame.add((delta) => {

      if (gyroEnabled) {
        const betaOrient = window.orientation === -90 ? -1 : 1
        const gammaOrient = window.orientation === -90 ? 1 : -1        
        gravity.set(
          betaOrient * 1.5 * beta / 90,
          gammaOrient * 1.5 * gamma / 90,
        )
      } else {
        gravity.x = this.pad.xValue * 1
        gravity.y = this.pad.yValue * 1
      }


      let tx = -orange.x + game.width * .5
      if (tx > game.viewport.left) {
        tx = game.viewport.left
      } else if (tx < -levelWidth + game.width) {
        tx = -levelWidth + game.width
      }
      
      levelContainer.x += (tx - levelContainer.x) * 0.1

      world.setGravity(gravity)
      world.step(delta)

      diff.set(orange.x, orange.y)
      orange.position.copy(orangeBody.getPosition())
      orange.position.x /= PSCALE
      orange.position.y /= PSCALE
      diff.x -= orange.x
      diff.y -= orange.y

      orange.applyRotation(diff.x, diff.y)


      orangeShadow.position.copy(orange.position)
      updateZIndex(orange)
      
      
      if (onLevelComplete && orange.x > orangeLevelEnd) {
        onLevelComplete(timer.time)
        onLevelComplete = null
      }
      
    })
  }

  destroy(options) {    
    window.removeEventListener('deviceorientation', this._handleDeviceOrientation)
    this._onEnterFrame.detach()

    super.destroy(options)
  }
}
