import * as THREE from 'three'
import * as PIXI from 'pixi.js'
import sceneAsset from '@/assets/scene/gold.json'
import game from '@/game'
import Sprite from '@/game/z/Sprite'
import cubemap from '@/assets/cubemap/cubemap.json'

const X_AXIS = new THREE.Vector3(0, 0, 1)
const Y_AXIS = new THREE.Vector3(-1, 0, 0)
const TO_RADIANS = 0.02
export default class Orange extends Sprite {
  constructor() {

    const textureWidth = 128
    const textureHeight = 128
    const spriteWidth = 128
    const spriteHeight =  128


    const renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true })

    renderer.setClearColor(0x000000)
    renderer.setClearAlpha(0)
    renderer.setSize(textureWidth, textureHeight)    
    
    const texture = PIXI.Texture.fromCanvas(renderer.domElement, PIXI.SCALE_MODES.LINEAR)
    texture.baseTexture.mipmap = false
    super(texture)

    this.scale.set(spriteWidth / textureWidth, spriteHeight / textureHeight)
    this.scene = sceneAsset.object

    this.renderer = renderer
    this.camera = this.scene.getObjectByName('PerspectiveCamera')      
    this.orange = this.scene.getObjectByName('Group')

    this.scene.getObjectByName('SpotLight').intensity = 5
    this.orange.children[0].material.envMap = cubemap.texture 


    this.scene.background = null
    this.camera.aspect = textureWidth / textureHeight
    this.camera.fov = 59.5
    this.camera.updateProjectionMatrix()

    
    this.anchor.set(.5,.5)

    this._onEnterFrame = game.onEnterFrame.add(this.update, this)
    this.renderTexture()
  }

  destroy(options) {
    this._onEnterFrame.detach()
    
    super.destroy(options)
  }

  applyRotation(x, y) {
    
    if (x !== 0 || y !== 0) {

      this.orange.rotateOnWorldAxis(X_AXIS, x * TO_RADIANS)
      this.orange.rotateOnWorldAxis(Y_AXIS, y * TO_RADIANS)

      this.render = true
    }
  }


  update() {
    if (this.render) {
      this.renderTexture()

      this.render = false  
    }    
  }

  renderTexture() {
    this.renderer.render(this.scene, this.camera)    
    this.texture.update()
    
    
  }
}