import Sprite from '@/game/z/Sprite'

export default function createSegment(data, atlas) {
  
  return {
    background: createSprites(data.background, atlas),
    foreground: createSprites(data.foreground, atlas),    
    items: createSprites(data.items, atlas)
  }
  
  
}

/**
 * Creates sprites from data array
 */
function createSprites(dataArray, atlas) {
  return dataArray.map(data => {
    const tex = atlas.getTexture(data.name)
    const sprite = new Sprite(tex)
    sprite.position.set(data.left, data.top)
    return sprite
  }).reverse()
}
