import 'core-js/stable'
import 'regenerator-runtime'

import * as PIXI from 'pixi.js'

import assets from '@/assets'
import images from '@/assets/images'
import game from '@/game'
import GoScene from '@/scene/GoScene'
import SensorScene from './scene/SensorScene'

window.MiratorgGame = {
  forceLevel: null,
  async bootstrap(canvasElement, onProgress) {
    
    // bootstrap    
    PIXI.loader.on('progress', () => {
      if (PIXI.loader.progress !== 100) {
        onProgress(PIXI.loader.progress)
      }
    })
    await assets.load(PIXI.loader) 

    images.baseTextures.forEach(texture => texture.mipmap = false)

    game.create(canvasElement, 1920, 1080)

    // resize stuff for iOS safari mostly :)
    const ATTEMPTS = 10

    const size = { w: 0, h: 0 }  
    let attempts = 0
    let attemptsInterval = 0

    const resizeAttempt = (event = null) => {
      if (size.w !== window.innerWidth || size.h !== window.innerHeight) {
        size.w = window.innerWidth
        size.h = window.innerHeight
        game.resize(size.w, size.h)

        const isSamsung = /SamsungBrowser/.test(navigator.userAgent)
        if (isSamsung) {
          canvasElement.style.width = '100%'
          canvasElement.style.height = '100%'
        } else {
          canvasElement.style.width = size.w + 'px'
          canvasElement.style.height = size.h + 'px'
        }
        
        
      }

      if (event) {
        clearInterval(attemptsInterval)
        attempts = ATTEMPTS
        attemptsInterval = setInterval(resizeAttempt, 50)
      } else {
        if (attempts-- < 0) {
          clearInterval(attemptsInterval)
        }
      }
    }

    window.addEventListener('resize', resizeAttempt)
    resizeAttempt()

    onProgress(100)
  },
  start(onLevelComplete) {
    while (game.stage.children.length > 0) {
      game.stage.children[0].destroy({ children: true })
    }

    const actuallyStart = () => {
      game.stage.addChild(new SensorScene(onLevelComplete))
    }

    if (!GoScene.permissionGranted) {
      game.stage.addChild(new GoScene(actuallyStart))
    } else {
      actuallyStart()
    }
  }
  
}