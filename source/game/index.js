import * as PIXI from 'pixi.js'
import Signal from 'mini-signals'
import tween from '@/tween'

class Game {
  constructor() {
    
    this.resize = this.resize.bind(this)
  }

  /**
   * Creates game, start renderer
   * @param {CanvasElement} canvas 
   * @param {number} width game pixels
   * @param {number} height game pixels
   */
  create(canvas, width, height) {
    this.time = 0
    this.width = width
    this.height = height

    this.app = new PIXI.Application({ 
      view: canvas,
      forceFXAA: false,
      forceCanvas: false,
      antialias: false,
      powerPreference: 'high-performance'
    })    

    this.app.ticker.add(this.update, this)    

    this.viewport = new PIXI.Rectangle(0, 0, 0, 0)
    this.ticker = this.app.ticker
    this.ticker.minFPS = 30

    this.renderer = this.app.renderer
    this.mouse = this.app.renderer.plugins.interaction.mouse

    this.pointer = new PIXI.Point()

    // flip app.stage with root
    // display tree: root > stage > game
    this.root = this.app.stage
    this.stage = this.root.addChild(new PIXI.Container())

    this.onResize = new Signal()
    this.onEnterFrame = new Signal()

    // TODO: tween to seconds?
    tween.getTime = () => this.time * 1000   
  }


  addChild(...children) {
    return this.stage.addChild(...children)
  }

  update() {
    const delta = this.ticker.deltaTime / 60
    this.time += delta

    tween.update()
    
    this.onEnterFrame.dispatch(delta)
  }

  resize(viewWidth, viewHeight) {

    const rotated = viewWidth < viewHeight
    const pixelRatio = window.devicePixelRatio || 1
    if (rotated) {
      const resolution = { width: viewWidth * pixelRatio, height: viewHeight * pixelRatio }
      this.renderer.resize(resolution.width, resolution.height)

      

      this.stage.scale.x =
      this.stage.scale.y = Math.min(resolution.height / this.width, resolution.width / this.height)    
      this.stage.x = resolution.width - (resolution.width - this.height * this.stage.scale.y) * .5
      this.stage.y = (resolution.height - this.width * this.stage.scale.x) * .5
      this.stage.rotation = Math.PI * .5


      const viewport = this.viewport
      viewport.x = -this.stage.y / this.stage.scale.y
      viewport.y = -this.stage.x / this.stage.scale.x
      viewport.width = this.width + Math.abs(viewport.x * 2)
      viewport.height = this.height + Math.abs(viewport.y * 2)
    } else {
      const resolution = { width: viewWidth * pixelRatio, height: viewHeight * pixelRatio }
      this.renderer.resize(resolution.width, resolution.height)

      this.stage.rotation = 0
      this.stage.scale.x =
      this.stage.scale.y = Math.min(resolution.width / this.width, resolution.height / this.height)    
      this.stage.x = (resolution.width - this.width * this.stage.scale.x) * .5
      this.stage.y = (resolution.height - this.height * this.stage.scale.y) * .5


      const viewport = this.viewport
      viewport.x = -this.stage.x / this.stage.scale.x
      viewport.y = -this.stage.y / this.stage.scale.y
      viewport.width = this.width + Math.abs(viewport.x * 2)
      viewport.height = this.height + Math.abs(viewport.y * 2)
    }
    
    


    this.onResize.dispatch()
  }
}

export default new Game()
export const eventBus = new PIXI.utils.EventEmitter()
