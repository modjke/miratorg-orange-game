import * as PIXI from 'pixi.js'

export default class Container extends PIXI.Container {
  constructor() {
    super()

    this.sortableChildren = false
    this.sortDirty = false
  }

  updateTransform() {
    if (this.sortableChildren && this.sortDirty) {
      this.sortChildren();
    }

    super.updateTransform()
  }

  sortChildren() {
    let sortRequired = false;

    for (let i = 0, j = this.children.length; i < j; ++i) {
      const child = this.children[i];

      child._lastSortedIndex = i;

      if (!sortRequired && child.zIndex !== 0) {
        sortRequired = true;
      }
    }

    if (sortRequired && this.children.length > 1) {
      this.children.sort(sortChildren);
    }

    this.sortDirty = false;
  }

  addChild(...children) {

    this.sortDirty = true

    return super.addChild(...children)
  }

  destroy(options) {
    this.sortDirty = false

    super.destroy(options)     
  }

}

function sortChildren(a, b)
{
    if (a.zIndex === b.zIndex)
    {
        return a._lastSortedIndex - b._lastSortedIndex;
    }

    return a.zIndex - b.zIndex;
}
