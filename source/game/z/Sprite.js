import * as PIXI from 'pixi.js'

export default class Sprite extends PIXI.Sprite {
  constructor(texture) {
    super(texture)

  }

  get zIndex()
  {
      return this._zIndex;
  }

  set zIndex(value) // eslint-disable-line require-jsdoc
  {
      this._zIndex = value;
      if (this.parent)
      {
          this.parent.sortDirty = true;
      }
  }


}