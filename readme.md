# Miratorg Orange

### Building

Requires node:10

```
npm i
npm run build
```

### API

```javascript
/**
 * Force certain level instead of random one
 * Optional, assign null to cancel
 */
MiratorgGame.forceLevel = 5

/**
 * Bootstraps a game engine (loads resources & initializes renderer)
 * Should be called once
 * @param {HTMLElement} canvasElement
 * @param {function} onProgress(progress) progress is [0...100], 
 * onProgress with progress = 100 guarantied to be called only once
 */
MiratorgGame.bootstrap(canvasElement, onProgress)

/**
 * Starts OR restarts a game, can be called anytime after bootsrap is complete
 * @param {function} onLevelComplete(seconds) seconds:float is a time elapsed since level was started
 */
MiratorgGame.start(onLevelComplete)
```

### Usage example
```javascript

// game canvas
var canvasElement = document.querySelector('#viewport');

MiratorgGame.bootstrap(canvasElement, function (progress) {
  // update UI/HTML with progress here
  
  // when we reach 100 (guarantied to be called once with 100% progress)
  if (progress === 100) {    
    // start a game, callback is called then it's done (seconds - time it took to complete a level)
    function onLevelComplete(seconds) {
        // restart immidiately when it's done
      MiratorgGame.start(onLevelComplete)
    }

    MiratorgGame.start(onLevelComplete)
  }  
});


```

### In order to iOS resize to work as expected

**Do not use iframe!**

```html
<!-- viewport-fit is super important -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, viewport-fit=cover">
```

```css
body {
  width: 100vw;
  height: 100vh;
  overflow: hidden;

  width: 100%; /* compat */
  height: 100%; /* compat */
}

canvas#viewport {
  position: absolute;
  top: 0;
  left: 0;
  /* width, height a set using js */
  /* unable to create a 'fullscreen' game canvas on ios with pure css */
}
```