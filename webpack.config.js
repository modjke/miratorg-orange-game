const webpackConfig = require('./webpack/index.js'),
  path = require('path'),
  DuplicatePackageCheckerPlugin = require('duplicate-package-checker-webpack-plugin'),
  merge = require('webpack-merge').smart

module.exports = (env, argv) => {
  const common = webpackConfig(__dirname, env, argv)

  const additional = {
    output: {
      filename: 'miratorg-game.js'
    },
    resolve: {
      alias: {
        '@': path.resolve(__dirname, './source')
      },
    },
    plugins: [
      // errors if duplicate packages added
      new DuplicatePackageCheckerPlugin({verbose: true, emitError: true})
    ],
  }

  return merge(common, additional)
}