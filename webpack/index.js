require('dotenv').config()

const fs = require('fs'),
	assert = require('assert'),	
	HtmlWebpackPlugin = require('html-webpack-plugin'),
	CleanWebpackPlugin = require('clean-webpack-plugin'),
  assets = require('./assets'),
	path = require('path'),
	pug = require('./pug'),
	babel = require('./babel'),
	css = require('./css'),
	images = require('./images'),
	merge = require('webpack-merge'),	
	fonts = require('./fonts'),
	copy = require('./copy'),	
	ImageminPlugin = require('./imageMin')

module.exports = (dirname, env, argv) => {
	const mode = argv.mode ? argv.mode : 'development',
		isDevMode = mode === 'development'


	const sourceDir = path.resolve(dirname, './source'),
		    staticDir = path.resolve(dirname, './static')

	assert(fs.existsSync(sourceDir),
		'There is no "source" directory in root project folder, although it is required!')
	assert(fs.existsSync(staticDir),
		'There is no "static" directory in root project folder, although it is required!')

	const PATH = {
		source: sourceDir,
		static: staticDir,
		output: path.resolve(dirname, './build'),
		artifacts: path.resolve(dirname, './artifacts'),
	}

	const entry = argv.entry ? argv.entry : path.join(PATH.source, 'index.js')

	const common = {
		mode,
		devtool: isDevMode ? 'inline-source-map' : false,
		entry: entry,
		output: {
			filename: '[name].[hash].js',
			chunkFilename: '[name].[chunkhash].js',
			publicPath: '',
			path: PATH.output,
		},
		plugins: [
			new CleanWebpackPlugin([PATH.output], { root: dirname, verbose: false }),
			new HtmlWebpackPlugin({
				template: path.join(PATH.source, 'index.pug')
			})
		],
		performance: {
			hints: false,
		},
    resolve: {
      mainFiles: ['index'],
      extensions: ['.js']
    }
	}


	const config = merge(
		common,
		babel(isDevMode),    
		pug(),
		css(),
		fonts(),		
		images(),		
		copy([
			PATH.static,
		]),
    assets(path.join(sourceDir, 'assets')),
		ImageminPlugin(isDevMode)
	)

  return config
}
