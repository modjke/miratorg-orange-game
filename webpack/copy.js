const CopyWebpackPlugin = require('copy-webpack-plugin')
module.exports = (patterns) => {
    return {
        plugins: [
            new CopyWebpackPlugin(patterns, {})
        ]
    }
}