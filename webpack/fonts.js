module.exports = function (){
    return {
        module: {
            rules: [
                {
                    test: /\.(woff|woff2|eot|ttf)$/i,
                    loader: "file-loader?name=fonts/[name]-[hash].[ext]"
                }
            ]
        }
    };
};