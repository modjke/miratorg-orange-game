module.exports = (isDevMode) => {
	const plugins = [
		'@babel/plugin-proposal-class-properties'
	]

	if (!isDevMode) {
		plugins.push(
			'babel-plugin-transform-remove-debugger',
			'babel-plugin-transform-remove-console')
	}

	return {
		module: {
			rules: [
				{
					test: /\.js$/,
					exclude: /node_modules/,
					use: [
						{
							loader: 'babel-loader',
							options: {
								babelrc: false,    
								presets: [
									[                    
										'@babel/preset-env',
										{
                      useBuiltIns: 'entry',
                      corejs: 3,
											targets: '>0.2%,not dead',											
										},
									],
								],
								comments: true,
								plugins,
							},
						},
            // 'eslint-loader'
					],
				},
			],
		},
	}
}
