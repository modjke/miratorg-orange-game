const path = require('path')

module.exports = {
  test: /\.(mp3|wav)$/,
  skipPixiMiddleware: true,
  assetClassModule: path.resolve(__dirname, '../assets/HowlAsset.js').replace(/\\/g, '/'),
  async process(content, url, context) {    
    context.emitFile(url, content)

    return { url, type: 'howl' }
  }
}