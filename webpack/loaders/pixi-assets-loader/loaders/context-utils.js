const promisify = require('util').promisify,
      readFile = promisify(require('fs').readFile)

module.exports = {
  async pipeFile(context, path, url) {
    context.emitFile(url, await readFile(path))
  }
} 