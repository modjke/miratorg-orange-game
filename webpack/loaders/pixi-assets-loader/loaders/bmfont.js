const util = require('util'),
  path = require('path'),
  xml2js = require('xml2js'),
  xmlParse = util.promisify(xml2js.parseString),
  readFile = util.promisify(require('fs').readFile)

const fontRegistry = {}
module.exports = {
  test: /\.fnt$/,
  async process(content, outputUrl, context) {

    const url = outputUrl.replace(/fnt$/, 'xml')

    context.emitFile(url, content)

    const resourceDir = path.dirname(context.resourcePath)
    const fontXml = await xmlParse(content.toString())
    const images = fontXml.font.pages[0].page.map(page => page['$'].file)
    const fontFace = fontXml.font.info[0]['$'].face

    for (const key of Object.keys(fontRegistry)) {
      if (key !== url && fontRegistry[key] === fontFace) {
        throw new Error(`Font ${fontFace} is already registered`)        
      }
    }

    fontRegistry[url] = fontFace

    for (const img of images) {
      const content = await readFile(path.join(resourceDir, img))
      context.emitFile(path.dirname(url) + '/' + img, content)       
    }

    return { url, type: 'bmfont' }
  }
}