module.exports = {
  test: /\.json$/,
  async process(content, url, context) {
    const data = JSON.parse(content.toString())
    const isThreeObject = data.metadata && data.metadata.type === 'Object' && data.metadata.generator === 'Object3D.toJSON'
    if (isThreeObject) {
      context.emitFile(url, content)      

      return { url, type: 'three-object' }
    } else {
      return null
    }

  }
}