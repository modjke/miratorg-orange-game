const promisify = require('util').promisify,
      readFile = promisify(require('fs').readFile)

module.exports = {
  test: /\.json$/,
  async process(content, url, context) {
    const data = JSON.parse(content.toString())
    const isSpine = data.skeleton && data.skeleton.spine
    if (isSpine) {
      context.emitFile(url, content)
      context.emitFile(url.replace(/json$/, 'atlas'), await readFile(context.resourcePath.replace(/json$/, 'atlas')))
      context.emitFile(url.replace(/json$/, 'png'), await readFile(context.resourcePath.replace(/json$/, 'png')))

      return { url, type: 'spine' }
    } else {
      return null
    }

  }
}