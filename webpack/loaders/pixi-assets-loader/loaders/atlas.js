const { promisify } = require('util'),
      path = require('path'),
      Packer = require('maxrects-packer').MaxRectsPacker,
      { PNG } = require('pngjs'),
      fs = require('fs'),
      readdir = promisify(fs.readdir),
      sizeOf = promisify(require('image-size'))
  

const RES_MAX = 4096

module.exports = {
  test: /atlas\.gen$/,
  assetClassModule: path.resolve(__dirname, '../assets/TextureAtlasAsset.js').replace(/\\/g, '/'),
  async process(content, url, context) {
    const production = context.mode === 'production'


    const dir = path.dirname(context.resourcePath)

    context.addContextDependency(dir)

    const outputBasePath = path.dirname(url)

    const supportedRe = /\.png$/
    const images = (await readdir(dir))
        .filter(file => supportedRe.test( path.extname(file).toLowerCase()))
        .map(image => path.join(dir, image))

    const packer = new Packer(RES_MAX, RES_MAX, 2, {
        smart: true,
        pot: true,
        square: false
    })

    const inputs = await Promise.all(images.map(image => sizeOf(image)))
    for (let i in inputs) {
        const item = inputs[i]
        if (!item.width || !item.height) {
            throw new Error(`[TextureAtlas] Unable to determine image size of ${images[i]}, got ${item.width}, ${item.height}`)            
        }

        if (item.width > RES_MAX || item.height > RES_MAX) {
            throw new Error(`[TextureAtlas] Image size is bigger than max resolution (${RES_MAX}): ${images[i]} is ${item.width}x${item.height} `)            
        }

        item.data = {
            path: images[i],
            frame: path.basename(images[i])
        }
    }


    packer.addArray(inputs)

    let atlasJson = {}


    for (const index in packer.bins) {
        const bin = packer.bins[index]

        const outputImagePath = `${outputBasePath}_${lz(index)}.png`

        if (!production) { 
            console.log(`[TextureAtlas] Generating ${outputImagePath}...`)
        }

        atlasJson = appendBin2Atlas(atlasJson, bin, path.basename(outputImagePath))

        const pngBuffer = await bin2png( bin )

        
        if (!production) { 
            console.log(`[TextureAtlas] Saving ${outputImagePath}...`)
        }
        
        context.emitFile(outputImagePath, pngBuffer)
    }

    const outputJsonPath = outputBasePath + '.json'
    context.emitFile(outputJsonPath, JSON.stringify( atlasJson ))


    return { url: outputJsonPath, type: 'atlas' }
  }
}

// converts number to string, adds leading zero
function lz(n) {
  if (n < 10) {
      return '0' + String(n)
  } else {
      return String(n)
  }
}

function appendBin2Atlas (atlasJson, bin, image) {
  const out = Object.assign({
      sprites: {},
      images: [],
      loader: 'atlas'
  }, atlasJson)

  out.images.push(image)

  for (const rect of bin.rects) {
      out.sprites[rect.data.frame] = {
          name: rect.data.path,
          frame: {
              x: rect.x,
              y: rect.y,
              w: rect.width,
              h: rect.height
          },
          image: image
      }
  }

  return out
}

function readPng(path) {
  return new Promise((resolve, reject) => {
      const png = new PNG()
      fs.createReadStream(path)
          .pipe(png)
          .on('parsed', () => {
              resolve(png)
          })
  })
}

function streamToBuffer(stream) {
  return new Promise((resolve, reject) => {
      let length = 0
      const chunks = []
      stream
          .on('data', (chunk) => {
              length += chunk.length
              chunks.push(chunk)
          })
          .on('end', () => {
            const buffer = Buffer.alloc(length)
            let offset = 0
            for (const chunk of chunks) {
              chunk.copy(buffer, offset, 0, chunk.length)
              offset += chunk.length
            }

           resolve(buffer)
          })
  })
}

function bitblt(path, dst, x, y) {
    return new Promise((resolve, reject) => {
      fs.readFile(path, (error, data) => {
        const parsed = PNG.sync.read(data)
        const png = new PNG()
        png.data = parsed.data
        png.width = parsed.width
        png.height = parsed.height
        png.gamma = parsed.gamma
        png.bitblt(dst, 0, 0, png.width, png.height, x, y)
  
        resolve()
      })
    })
  }
  

async function bin2png(bin) {
  const out = new PNG({
      width: bin.stage.width,
      height: bin.stage.height,
      colorType: 6
  })


  for (const rect of bin.rects) {
    await bitblt(rect.data.path, out, rect.x, rect.y)
  }

  return await streamToBuffer(out.pack());
}
