const contextUtils = require('./context-utils'),
      path = require('path')

module.exports = {
  test: /\.json$/,
  async process(content, url, context) {
    const data = JSON.parse(content.toString())
    const isThreeCubemap = data.type === 'three-cubemap'
    if (isThreeCubemap) {
      context.emitFile(url, content)

      const imageBaseUrl = path.dirname(url) + '/'
      const imagesDir = path.dirname(context.resourcePath)
      for (const image of data.images) {        
        await contextUtils.pipeFile(context, path.join(imagesDir, image), imageBaseUrl + image)
      }

      return { url, type: 'three-cubemap' }
    } else {
      return null
    }
  }
}