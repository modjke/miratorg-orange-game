module.exports = {
  test: /\.json$/,
  async process(content, url, context) {
    context.emitFile(url, content)
    return { url, type: 'data' }
  }
}