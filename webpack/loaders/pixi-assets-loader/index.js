const path = require('path')
const loaderUtils = require('loader-utils')

// order matters here
// first to handle asset 
const assetLoaders = [
  require('./loaders/bmfont'),
  require('./loaders/howl'),
  require('./loaders/atlas'),
  require('./loaders/spine'),
  require('./loaders/three-object'),
  require('./loaders/three-cubemap'),
  require('./loaders/data')
]



const loader = async function (content) {
  if (!this.emitFile)
      throw new Error('\n\nemitFile is required from module system');

  const middlewareInstallModule = path.resolve(__dirname, 'middleware/install.js').replace(/\\/g, '/')

  const callback = this.async()
  const options = loaderUtils.getOptions(this)
  
  if (!options.assetsDir || !path.isAbsolute(options.assetsDir)) {
    callback(new Error('Absolute assetsDir option should be provided'))
    return
  }

  for (const loader of assetLoaders) {    
    if (!loader.test.test(this.resourcePath))
      continue

    const resourcePathRelative = path.relative(options.assetsDir, this.resourcePath)
    if (path.isAbsolute(resourcePathRelative)) {
      callback(new Error(`Unable to determine relative resource path, are you importing from assetsDir?`))
      return
    }

    const outputUrl = path.join('assets', resourcePathRelative).replace(/\\/g, '/') // TODO: configurable?
    const asset = await loader.process(content, outputUrl, this)
    
  
    if (asset) {
      if (!asset.url || !asset.type) {
        throw new Error(`loader.process returned null url or type`)
      }

      const assetClassModule = loader.assetClassModule
      const assetDeclaration = assetClassModule ? `
        import Asset from '${assetClassModule}'
        const asset = new Asset('${asset.url}', '${asset.type}')
      ` : `
        const asset = {
          url: '${asset.url}',
          type: '${asset.type}'
        }
      `

      callback(null, `
        import '${middlewareInstallModule}'

        ${assetDeclaration}
        
        // creates recursive self reference
        // used in a middleware to access asset instance
        // removed in afterMiddleware        
        // @see middleware/install.js        
        asset.metadata = { asset } 

        export default asset
      `)

      
      return
    }
  }

  if (!/\.js$/.test(this.resourcePath)) {
    this.emitWarning(new Error(`Could not find a suitable loader for resource: ${this.resourcePath}`))
  }
  

  callback(null, content)
}


loader.raw = true

module.exports = loader