import * as PIXI from 'pixi.js'

export default class TextureAtlasAsset {
  constructor(url, type) {
    this.url = url
    this.type = type
    this.textures = null  // assigned by middleware after load
  }

  
  texturesWithPrefix(prefix) {
    return Object.keys(this.textures)
        .filter(name => name.indexOf(prefix) === 0)
        .sort()
        .map(name => this.textures[name])
  }

  texturesThatMatch(re) {
    return Object.keys(this.textures)
        .filter(name => re.test(name))
        .sort()
        .map(name => this.textures[name])
  }

  getTexture(name) {
    const tex = this.textures[name] || this.textures[name + '.png'] || this.textures[name + '.jpg']
    if (!tex) {
        throw new Error(`Texture "${name}" does not exist`)
    }
    return tex
  }

  /**
   * @deprecated
   */
  createSprite(textureName) {
    return new PIXI.Sprite(this.getTexture(textureName))
  }

  get baseTextures() {
    const out = []
    for (const { baseTexture } of Object.values(this.textures)) {
      if (out.indexOf(baseTexture) === -1) {
        out.push(baseTexture)
      }
    }
    return out 
  }

}