export default class HowlAsset {
  constructor(url, type) {
    this.url = url
    this.type = type    
    this.howl = null  // assigned by middleware after load
  }
}