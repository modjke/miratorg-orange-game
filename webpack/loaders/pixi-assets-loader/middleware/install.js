import * as PIXI from 'pixi.js'

import NoopLoadElement from './NoopLoadElement'
import howl from './howl'
import bmfont from './bmfont'
import atlas from './atlas'
import data from './data'
import spine from './spine'
import threeObject from './three-object'
import threeCubemap from './three-cubemap'

const middlewareMap = {
  'howl': howl,
  'bmfont': bmfont,
  'atlas': atlas,
  'data': data,
  'spine': spine,
  'three-object': threeObject,
  'three-cubemap': threeCubemap
}

registerBeforeMiddleware(prepareResource)
registerMiddleware(loadResource)
registerMiddleware(cleanup)

function prepareResource(resource, next) {
  const { asset } = resource.metadata
  if (asset) {
    switch (asset.type) {
      case 'howl':
        resource.metadata.loadElement = new NoopLoadElement()
        break
    }      
  }

  next()
}


function loadResource(resource, next) {
  const loader = this
  const asset = resource.metadata.asset

  if (asset) {    
    const middleware = middlewareMap[asset.type]
    if (!middleware) {
      throw new Error(`Unknown asset type: ${asset.type}`)
    }

    middleware.call(loader, resource, asset, next);   
  } else {
    next()
  }
}

function cleanup(resource, next) {
  // remove recursive self reference in asset
  if (resource.metadata.asset && 
      resource.metadata.asset.metadata === resource.metadata) {
    delete resource.metadata.asset.metadata
  }

  next()
}

function registerBeforeMiddleware(middleware) {
  PIXI.loaders.Loader._defaultAfterMiddleware.push(middleware)
  if (PIXI.loader && PIXI.loader.use) {
    PIXI.loader.pre(middleware)
  }
}

function registerMiddleware(middleware) {
  PIXI.loaders.Loader.addPixiMiddleware(() => middleware)
  if (PIXI.loader && PIXI.loader.use) {
    PIXI.loader.use(middleware)
  }
}
