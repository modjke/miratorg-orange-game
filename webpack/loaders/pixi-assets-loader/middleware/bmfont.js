
export default function (resource, asset, next) {

  asset.name = resource.bitmapFont.font
  asset.size = resource.bitmapFont.size

  next()

}