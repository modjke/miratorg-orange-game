export default class NoopLoadElement {
  constructor() {
  }

  addEventListener(event, handler) {
    if (event === 'load') {
      setTimeout(handler, 0)
    }
  }

  removeEventListener(event, handler) {
  }

  appendChild() {
  }

  load() {
    // do nothing
  }
}