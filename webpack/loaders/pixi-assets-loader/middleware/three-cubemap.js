import { CubeTextureLoader } from 'three'

export default function (resource, asset, next) {
  const baseUrl = resource.url.substr(0, resource.url.lastIndexOf('/') + 1)
  new CubeTextureLoader()
    .setPath(baseUrl)
    .load(resource.data.images, texture => {
      asset.texture = texture
      next()
    })
}