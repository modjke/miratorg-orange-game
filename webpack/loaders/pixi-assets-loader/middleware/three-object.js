import { ObjectLoader } from 'three'


export default function (resource, asset, next) {
  new ObjectLoader().parse(resource.data, object => {
    asset.object = object

    next()
  })
  
}