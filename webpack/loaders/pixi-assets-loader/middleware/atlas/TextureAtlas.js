import * as PIXI from 'pixi.js'

const { Rectangle, Texture } = PIXI;


/**
 * TextureAtlas for PIXI v4 to support multiple images in one JSON
 * Mostly copied from PIXI original one
 * 
 * TODO: used only for parsing, move to atlas middleware as function?
 */
export default class TextureAtlas {

    static parse(baseTexturesMap, sprites, callback) {
        const atlas = new TextureAtlas(baseTexturesMap, sprites)
        atlas._parse(() => callback(atlas))
    }

    /**
     * The maximum number of Textures to build per process.
     *
     * @type {number}
     * @default 1000
     */
    static get BATCH_SIZE()
    {
        return 1000;
    }

    /**
     * @param {Object} baseTexturesMap Reference to the source BaseTexture map object.
     * @param {Array} sprites - TextureAtlas image data.
     */
    constructor(baseTexturesMap, sprites)
    {
        this._baseTexturesMap = baseTexturesMap
        this._sprites = sprites
        this._spriteKeys = Object.keys(sprites)

        this.textures = {}
        this.baseTextures = Object.values(baseTexturesMap)
    }

    /**
     * Parser TextureAtlas from loaded data. This is done asynchronously
     * to prevent creating too many Texture within a single process.
     *
     * @param {Function} callback - Callback when complete returns
     *        a map of the Textures for this TextureAtlas.
     */
    _parse(callback)
    {
        this._batchIndex = 0;
        this._callback = callback;

        if (this._spriteKeys.length <= TextureAtlas.BATCH_SIZE)
        {
            this._processSprites(0);
            this._parseComplete();
        }
        else
        {
            this._nextBatch();
        }
    }

    _processSprites(initialSpriteIndex)
    {
        let frameIndex = initialSpriteIndex;
        const maxSprites = TextureAtlas.BATCH_SIZE;

        while (frameIndex - initialSpriteIndex < maxSprites && frameIndex < this._spriteKeys.length)
        {
            const i = this._spriteKeys[frameIndex];
            const data = this._sprites[i];
            const rect = data.frame;

            if (rect)
            {
                let frame = null
                let trim = null
                const sourceSize = data.trimmed !== false && data.sourceSize
                    ? data.sourceSize : data.frame;

                const baseTexture = this._baseTexturesMap[data.image]
                const sourceScale = baseTexture.sourceScale;

                const orig = new Rectangle(
                    0,
                    0,
                    Math.floor(sourceSize.w * sourceScale) / baseTexture.resolution,
                    Math.floor(sourceSize.h * sourceScale) / baseTexture.resolution
                );

                if (data.rotated)
                {
                    frame = new Rectangle(
                        Math.floor(rect.x * sourceScale) / baseTexture.resolution,
                        Math.floor(rect.y * sourceScale) / baseTexture.resolution,
                        Math.floor(rect.h * sourceScale) / baseTexture.resolution,
                        Math.floor(rect.w * sourceScale) / baseTexture.resolution
                    );
                }
                else
                {
                    frame = new Rectangle(
                        Math.floor(rect.x * sourceScale) / baseTexture.resolution,
                        Math.floor(rect.y * sourceScale) / baseTexture.resolution,
                        Math.floor(rect.w * sourceScale) / baseTexture.resolution,
                        Math.floor(rect.h * sourceScale) / baseTexture.resolution
                    );
                }

                //  Check to see if the sprite is trimmed
                if (data.trimmed !== false && data.spriteSourceSize)
                {
                    trim = new Rectangle(
                        Math.floor(data.spriteSourceSize.x * sourceScale) / baseTexture.resolution,
                        Math.floor(data.spriteSourceSize.y * sourceScale) / baseTexture.resolution,
                        Math.floor(rect.w * sourceScale) / baseTexture.resolution,
                        Math.floor(rect.h * sourceScale) / baseTexture.resolution
                    );
                }


                this.textures[i] = new Texture(
                    baseTexture,
                    frame,
                    orig,
                    trim,
                    data.rotated ? 2 : 0,
                    data.anchor
                );
            }

            frameIndex++;
        }
    }


    /**
     * The parse has completed.
     *
     * @private
     */
    _parseComplete()
    {
        const callback = this._callback;

        this._callback = null;
        this._batchIndex = 0;
        callback.call(this, this.textures);
    }

    /**
     * Begin the next batch of textures.
     *
     * @private
     */
    _nextBatch()
    {
        this._processSprites(this._batchIndex * TextureAtlas.BATCH_SIZE);
        this._batchIndex++;
        setTimeout(() =>
        {
            if (this._batchIndex * TextureAtlas.BATCH_SIZE < this._spriteKeys.length)
            {
                this._nextBatch();
            }
            else
            {
                this._parseComplete();
            }
        }, 0);
    }

   
}
