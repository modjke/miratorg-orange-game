import url from 'url'
import TextureAtlas from './TextureAtlas'

export default function (resource, asset, next) {
  const loadOptions = {
    crossOrigin: resource.crossOrigin,
    metadata: resource.metadata.imageMetadata,
    parentResource: resource,
  };

  const { images, sprites } = resource.data

  if (typeof images === 'undefined')
    throw new Error(`Atlas ${resource.url} is malformed: no images`)

  if (typeof sprites === 'undefined')
    throw new Error(`Atlas ${resource.url} is malformed: no sprites`)

  const baseTexturesMap = {}
  let leftToLoad = images.length
  const pathUrl = resource.url.replace(this.baseUrl, '')

  for (let i = 0; i < images.length; i++) {
    const imageName = images[i]
    const imageResourceName = images[i] + '_atlas'
    const imageUrl = url.resolve(pathUrl, imageName)

    this.add(imageResourceName, imageUrl, loadOptions, function onImageLoaded(res) {
      if (res.error) {
        // if any image can not be loaded then atlas can not be constructed
        next(res.error)
    
        return
      }
    
      --leftToLoad
      baseTexturesMap[imageName] = res.texture.baseTexture
    
      if (leftToLoad === 0) {
    
        TextureAtlas.parse(baseTexturesMap, sprites, atlas => {                    
          asset.textures = atlas.textures // asset is TextureAtlasAsset
    
          next()
        })
      }
    })
  }
}

