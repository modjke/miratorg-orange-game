import { Howl } from 'howler'

export default function (resource, asset, next) {

  asset.howl = new Howl({
    src: [resource.url],
    preload: true,
    onload: next
  })

}