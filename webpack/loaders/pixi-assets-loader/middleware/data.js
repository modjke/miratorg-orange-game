
export default function (resource, asset, next) {
  Object.assign(asset, resource.data)

  next()
}