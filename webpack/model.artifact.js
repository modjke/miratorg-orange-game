const path = require('path'),
      merge = require('webpack-merge'),
      babel = require('./babel')


module.exports = (mode, index, build) => merge(
    {
        mode,
        target: 'node',
        entry: index,
        output: {
            path: path.dirname(build),
            filename: path.basename(build),
            libraryExport: 'default',
            libraryTarget: 'umd'
        }
    }, babel({
        node: 'current'
    })
)