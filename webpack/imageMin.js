const ImageminPlugin = require('imagemin-webpack-plugin').default

module.exports = (isDevMode) => {
	return {
		plugins: [
			new ImageminPlugin({
				disable: isDevMode,
				optipng: null,
				pngquant: {
					strip: true,
					speed: 3,
					verbose: true
				},
			}),
		],
	}
}
