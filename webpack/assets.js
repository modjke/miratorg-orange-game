const path = require('path')

module.exports = (assetsDir) => {
  if (!assetsDir) {
    throw new Error(`assetsDir should be provided`)
  }

  return {
    resolve: {
      mainFiles: ['atlas'],
      extensions: ['.gen']
    },
    module: {      
      rules: [
        {
          test: /./,
          type: 'javascript/auto',
          include: [assetsDir],
          use: {
            loader: path.resolve(__dirname, 'loaders/pixi-assets-loader'),
            options: {
              assetsDir
            }
          }          
        }
      ]
    }
  }
}