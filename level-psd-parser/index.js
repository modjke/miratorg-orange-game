
const fs = require('fs')
const path = require('path')
const inPsd = path.resolve(process.argv[2] || 'lvl.psd')

const psd = require('psd.js').fromFile(inPsd)


;(async function () {

  const veryGoodCode = path.resolve('./output')
  if (!fs.existsSync(veryGoodCode)) {
    fs.mkdirSync(veryGoodCode)
  }

  const outputPath = path.join(veryGoodCode, path.basename(inPsd, '.psd'))
  if (!fs.existsSync(outputPath)) {
    fs.mkdirSync(outputPath)
  } 

  psd.parse()
  
  const tree = psd.tree()
  const bodies = tree.childrenAtPath('bodies')[0]._children  

  const output = {
    width: tree.width,
    height: tree.height,
    name: 'level',
    static: [],
    items: [],
    foreground: [],
    background: []
  }

  for (const { coords } of bodies) {
    output.static.push(coords)
  }
  
  let index = 0

  function parseLayers(layers) {
    const out = []
    for (const layer of layers) {
    
      if (getImage(layer)) {
        const name = 'item_' + String(index++).padStart(3, '0') + '.png'
        out.push({
          name,
          ...layer.coords
        })

        savePng(layer, path.join(outputPath, name))
      }
    }
    return out
  }

  output.background = parseLayers(tree.childrenAtPath('background')[0]._children)
  output.foreground = parseLayers(tree.childrenAtPath('foreground')[0]._children)
  output.items = parseLayers(tree.childrenAtPath('items')[0]._children)
  

  
  fs.writeFileSync(path.join(outputPath, 'level.json'), JSON.stringify(output, null, '  '), 'utf8')
})()

function getImage(layer) {
  if (!layer) {
    return null
  }
  return layer.image || layer.layer.image
}

function savePng(layer, path) {
  getImage(layer).saveAsPng(path)
}
