module.exports = {
    'parser': 'babel-eslint',
    'env': {
        'browser': true,
        'es6': true
    },
    'extends': 'eslint:recommended',
    'globals': {
        '__BUILD_CONFIG__MODE': 'readonly',
        '__BUILD_CONFIG__API_URL': 'readonly',
        '__BUILD_CONFIG__GA_ID': 'readonly'
    },
    'parserOptions': {
        'ecmaVersion': 2018,
        'sourceType': 'module'
    },
    'rules': {
        'no-console': [ 'warn' ],
        'linebreak-style': [
            'error',
            'unix'
        ],
        'no-unused-vars': [
          'error',
          { 'args': 'none' }
        ]
    }
}
